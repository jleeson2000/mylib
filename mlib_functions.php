<?php
require("mlib_values.php");

function html_head($title) {
    echo '<html lang="en">';
    echo '<head>';
    echo '<meta charset="utf-8">';
    echo "<title>$title</title>";
    echo '<link rel="stylesheet" href="mlib.css">';
    echo '</head>';
    echo '<body>';
}

function try_again($str) {
    echo $str;
    echo "<br/>";
    echo '<a href="#" onclick="history.back(); return false;">Try Again</a>';
    require("mlib_footer.php");
    exit;
}

function validate_media($title, $author, $type, $description) {
    $error_messages = array();

    if(strlen($title) == 0) {
        array_push($error_messages, "Tool field must have a value.");
    }

    if(strlen($author) == 0) {
        array_push($error_messages, "Author field must have a value.");
    }

    if(strlen($type) == 0) {
        array_push($error_messages, "Type field must have a value.");
    }

    if(strlen($description) == 0) {
        array_push($error_messages, "Description field must have a value.");
    }

    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if(strleen($title) != 0) {
            $sql = "SELECT COUNT(*) FROM media WHERE title = '$title' AND status = 'active'";
            $result = $db->query($sql)->fetch();
            if($result[0] > 0) {
                array_push($error_messages, "Media title $title is not unique. Media title must be unique.");
            }
        }

        if(strleen($type) != 0) {
            $sql = "SELECT COUNT(*) FROM media WHERE type = '$type AND status = 'active'";
            $result = $db->query($sql)->fetch();
            if($result[0] == 0) {
                array_push($error_messages, "Media type $type is not defined. Media type must be valid.");
            }
        }
    } catch(PDOException $e) {
        echo "Exception: ".$e->getMessage();
        echo "<br/>";
        $db = NULL;
    }

    return $error_messages;
}

function MyCheckDate($postedDate) {
    if(preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $postedDate, $datebit)) {
        return checkdate($datebit[2], $datebit[3], $datebit[1]);
    } else {
        return false;
    }
}

function we_are_not_admin() {
    if(empty($_SESSION["valid_user"])) {
        echo "Only administrators can execute this function.<br/>";
        require("mlib_footer.php");
        return true;
    }
}
?>