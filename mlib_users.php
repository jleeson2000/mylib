<!doctype html>
<?php
require("mlib_functions.php");
html_head("mlib users");
require("mlib_header.php");
session_start();
require("mlib_sidebar.php");

if(we_are_not_admin()){
    exit;
}

if(!isset($_POST["submit"])) {
?>

<h2>Add User</h2>
<form action="mlib_users.php" method="post">
    <table border="0">
        <tr bgcolor="#cccccc">
            <td width="100">Field</td>
            <td widtth="300">Value</td>
        </tr>
        <tr>
            <td>First</td>
            <td align="left"><input type="text" name="first" size="35" maxlength="35"></td>
        </tr>
        <tr>
            <td>Last</td>
            <td align="left"><input type="text" name="last" size="35" maxlength="35"></td>
        </tr>
        <tr>
            <td>Email</td>
            <td align="left"><input type="text" name="email" size="70" maxlength="70"></td>
        </tr>
        <tr>
            <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
        </tr>
    </table>
</form>

<?php
} else {

    $first = $_POST["first"];
    $last = $_POST["last"];
    $email = $_POST["email"];

    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $first = trim($first);
        if(empty($first)) {
            try_again("First name cannot be empty.");
        }
        $last = trim($last);
        if(empty($last)) {
            try_again("Last name cannot bee empty.");
        }
        $email = trim($email);
        if(empty($email)) {
            try_again("Email cannot be empty.");
        }

        $sql = "SELECT COUNT(*) FROM users WHERE first = '$first' AND last = '$last'";
        $result = $db->query($sql)->fetch();
        if($result[0] > 0) {
            try_again($first." ".$last." is not unique. Names must be unique.");
        }

        $sql = "SELECT COUNT(*) from users WHERE email = '$email'";
        $result = $db->query($sql)->fetch();
        if($result[0] > 0){
            try_agani("$email is not unique. Email must be unique.");
        }

        $db->exec("INSERT INTO users (first, last, email) VALUES ('$first', '$last', '$email');");

        $last_id = $db->lastInsertId();

        print "<h2>User Added</h2>";
        print "<table border=1>";
        print "<tr>";
        print "<td>Id</td><td>First</td><td>Last</td><td>Email</td>";
        print "</tr>";
        $row = $db->query("SELECT * FROM users WHERE id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
        print "<tr>";
        print "<td>".$row['id']."</td>";
        print "<td>".$row['first']."</td>";
        print "<td>".$row['last']."</td>";
        print "<td>".$row['email']."</td>";
        print "</tr>";
        print "</table>";

        $db = NULL;

    } catch(PDOException $e){
        echo "Exception: ".$e->getMessage()."<br/>";
        $db = NULL;
    }
}
require("mlib_footer.php");
?>