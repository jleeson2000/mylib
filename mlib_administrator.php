<!doctype html>
<?php
require("mlib_functions.php");
html_head("mlib administrator");
require("mlib_header.php");
session_start();
require("mlib_sidebar.php");

if(we_are_not_admin()) {
    exit;
}

if(!isset($_POST["submit"])) {
    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>

        <h2>Change Administratioin Priviledges</h2>
        <form actoin="mlib_administrationo.php" method="post">
            <table border=1>
                <tr>
                    <td>Click one to Change</td><td>User</td><td>Login</td>
                </tr>
<?php
    $result = $db->query("SELECT * FROM users");
    foreach($result as $row) {
        print "<tr>";
        print "<td><input type='radio' name='id' value=".$row["id"]."></td>";
        print "<td>".$row["first"]." ".$row["last"]."</td>";
        print "<td>".$row["login"]."</td>";
        print "</tr>";
    }
?>
    </table>
    <p>Clicking an entry with a login will remove  administration priviledges.</p>
    <p>Clicking  an entry without a login enable administration priviledges. Enter login and password below:</p>
    Login: <input type="text" name="login"/><br/>
    Password: <input type="text" name="password"/><br/>
    <input type="submit" name="submit" value="Submit"/><br/>
    </form>
<?php

    $db = NULL;

} catch(PDOException $e) {
    echo "Exception: ".$e->getMessage()."<br/>";
    $db = NULL;
}

} else  {
?>

    <h2>Administration Priviledges Changes</h2>

<?php
    $id = $_POST["id"];
    $login = $_POST["login"];
    $password  = $_POST["password"];

    try {
        if(empty($id)) {
            echo "You did not select any  user to change priviledges.</br>";
        }  else {
            $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "SELECT * FROM users WHERE id = $id";
            $result = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            if(empty($result["login"])) {
                $login = trim($login);
                if(empty($login)) {
                    try_again("Login cannot be empty.");
                }
                $password = trim($password);
                if(strlen($password) < 8) {
                    try_again("Password must be at least 8 characters.");
                }

                $passwd = sha1($password);

                $sql = "SELECT COUNT(*) FROM users WHERE login = '$login'";
                $result = $db->query($sql)->fetch();
                if($result[0] > 0) {
                    try_again($login." is not unique. Login must be unique.");
                }
                $db->exec("UPDATE users SET login='$login', password='$passwd' WHERE id = $id");
            }else {
                $db->exec("UPDATE users SET login = NULL, password = NULL WHERE id = $id");
            }

            print "<table border=1>";
            print "<tr>";
            print "<td>User</td><td>Login</td>";
            print "</tr>";
            $sql = "SELECT * FROM users WHERE id = $id";
            $result = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            print "<tr>";
            print "<td>".$result["first"]." ".$result["last"]."</td><td>".$result["login"]."</td>";
            print "</tr>";
            print "</table>";
        }
        $db = NULL;
    }catch(PDOException $e){
        echo "Exception: ".$e->getMessage()."<br/>";
        $db = NULL;
    }
}
require("mlib_footer.php");
?>
