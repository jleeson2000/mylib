<!doctype html>
<?php
require("mlib_functions.php");
html_head("Process Media");
require("mlib_header.php");
session_start();
require("mlib_sidebar.php");

if(we_are_not_admin())  {
    exit;
}

echo "<h2>Processing Media from List...</h2>";

if($_FILES["userfile"]["error"] > 0) {
    echo "Problem: ";
    switch($_FILES["userfile"]["error"]) {
        case 1: echo "File exceeded upload_max_filesize"; break;
        case 2: echo "File exceeded max_file_size"; break;
        case 3: echo "File only partially uploaded"; break;
        case 4: echo "No file uploaded"; break;
    }
    exit;
}

if($_FILES["userfile"]["type"] != "text/plain") {
    echo "Problem: file is  not plain text";
    exit;
}

$upfile = "./uploads/".$_FILES["userfile"]["name"];

if(is_uploaded_file($_FILES["userfile"]["tmp_name"])) {
    if(!move_uploaded_file($_FILES["userfile"]["tmp_name"], $upfile)) {
        echo "Problem: Could not move file to destination directory";
        exit;
    }
} else {
    echo "Problem: Possible file upload attack. Filename: ";
    echo $_FILES["userfile"]["name"];
    exit;
}

echo "File uploaded successfully<br><br>";

$fp = fopen($upfile, 'r');

if(!$fp) {
    echo "<p>I could not open $upfile right now.</p>";
    exit;
}

while(!feof($fp)) {
    $line = fgets($fp, 60);
    $line_array = explode(',', $line);
    $title = trim($line_array[0]);
    $author = trim($line_array[1]);
    $type = trim($line_array[2]);
    $description = trim($line_array[3]);

    $errors = validate_equipment($title, $author, $type, $description);
    if(empty($errors)) {
        echo "title: $title <br/>";
        echo "author: $author <br/>";
        echo "type: $type <br/>";
        echo "description: $description <br/>";

        try {
            $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->exec("INSERT INTO media (title, author, type, status, user_id, description) VALUES ('$title', '$author', '$type', 'active', 0, '$description');");
            $db = NULL;
        } catch(PDOException $e) {
            echo "Exception: ".$e->getMessage();
            echo "<br/>";
            $db = NULL;
        }
    } else {
        echo "Errors found in media entry:<br/>";
        echo "title: $title<br/>";
        echo "author: $author<br/>";
        echo "type: $type<br/>";
        echo "description: $description<br/>";

        foreach($errors as $error) {
            echo $error."<br/>";
        }
    }
    echo "<br/>";
}

fclose($fp);
require("mlib_footer.php");
?>